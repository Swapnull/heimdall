/**
 * 
 * @param {Object|undefined} err - Error object or undefined
 * @param {*} successBody - The body we want to return
 * @param {*} callback - the dynamoDb callback
 */
module.exports = (err, successBody, callback) => {
    const data = err ? 
      { statusCode: 500, 
        headers: { "Access-Control-Allow-Origin" : "*" },
        body: JSON.stringify(err.code + ': ' + err.message)} 
    : { statusCode: 200, 
        headers: { "Access-Control-Allow-Origin" : "*" },
        body: JSON.stringify(successBody)};

    callback(null, data);    
  }