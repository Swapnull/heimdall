const aws = require('aws-sdk');
const luxon = require('luxon');
const uuid = require('uuid/v4');
const handleResponse = require('../../helpers/apiResponse')
const dynamoDb = new aws.DynamoDB.DocumentClient();
const TableName = 'recruiter';

module.exports.create = (req, context, callback) => {
  const data = JSON.parse(req.body);
  return dynamoDb.put({
    TableName,
    Item: {
        id: uuid(),
        name: data.name,
        company: data.company,
        phone: data.phone,
        email: data.email, 
        thoughts: data.thoughts,
        updated: luxon.DateTime.local().toString()
    }
  }, (err) => handleResponse(err, "Success", callback));
}
