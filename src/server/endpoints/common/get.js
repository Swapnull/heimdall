const aws = require('aws-sdk');
const handleResponse = require('../../helpers/apiResponse')
const dynamoDb = new aws.DynamoDB.DocumentClient();

module.exports.get = (req, context, callback) => 
  dynamoDb.scan({ TableName: req.path.split('/')[1] }, 
    (err, result) => handleResponse(err, result.Items, callback));

module.exports.getById = (req, context, callback) => 
  dynamoDb.get({
    TableName: req.path.split('/')[1],
    Key: {
      id: req.pathParameters.id,
    }
  }, (err, result) => handleResponse(err, result.Item, callback));
