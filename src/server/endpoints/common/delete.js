const aws = require('aws-sdk');
const handleResponse = require('../../helpers/apiResponse')
const dynamoDb = new aws.DynamoDB.DocumentClient();

module.exports.delete = (req, context, callback) => 
    dynamoDb.delete({
        TableName: req.path.split('/')[1],
        Key: { id: req.pathParameters.id }
    }, (err) => handleResponse(err, 'success', callback));
