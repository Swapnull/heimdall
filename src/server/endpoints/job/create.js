const aws = require('aws-sdk');
const luxon = require('luxon');
const uuid = require('uuid/v4');
const handleResponse = require('../../helpers/apiResponse')
const dynamoDb = new aws.DynamoDB.DocumentClient();
const TableName = 'job';

module.exports.create = (req, context, callback) => {
  const data = JSON.parse(req.body);
  return dynamoDb.put({
    TableName,
    Item: {
        id: uuid(),
        company: data.company,
        name: data.name,
        status: data.status,
        rate: data.rate,
        location: data.location,
        notes: data.notes,
        updated: luxon.DateTime.local().toString()
    }
  }, (err) => handleResponse(err, "Success", callback));
}
