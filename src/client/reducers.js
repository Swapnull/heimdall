import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import companyReducer from './components/Company/reducers';
import jobReducer from './components/Job/reducers';
import recruiterReducer from './components/Recruiter/reducers';

export default combineReducers({
    router: routerReducer,
    companies: companyReducer,
    jobs: jobReducer,
    recruiters:recruiterReducer
})