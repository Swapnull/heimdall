import { takeLatest } from "redux-saga/effects";
import * as company from './components/Company/sagas';
import * as job from './components/Job/sagas';
import * as recruiter from './components/Recruiter/sagas';

export function* watcherSaga() {
    yield takeLatest(company.GET_COMPANIES, company.fetchCompanies);
    yield takeLatest(company.DELETE_COMPANY, company.deleteCompany);
    yield takeLatest(company.ADD_COMPANY, company.addCompany);
    yield takeLatest(company.ADD_COMPANY_NOTE, company.addNote);
    yield takeLatest(company.UPDATE_COMPANY_SELECTED_TAB, company.updateSelectedTab);

    yield takeLatest(job.GET_JOBS, job.fetchJobs);
    yield takeLatest(job.DELETE_JOB, job.deleteJob);
    yield takeLatest(job.ADD_JOB, job.addJob);
    yield takeLatest(job.ADD_JOB_NOTE, job.addNote);

    yield takeLatest(recruiter.GET_RECRUITERS, recruiter.fetchRecruiters);
    yield takeLatest(recruiter.DELETE_RECRUITER, recruiter.deleteRecruiter);
    yield takeLatest(recruiter.ADD_RECRUITER, recruiter.addRecruiter);
}