// @flow

import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import createHistory from 'history/createBrowserHistory';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import ConnectedNav  from './components/Nav/';
/*import Footer from './components/Footer/';*/
import Companies from './components/Company/';
import CompanyDetail from './components/Company/detail';

import createStore from './store.js';
import './scss/main.scss';

const history = createHistory();
const store = createStore(history);
const persistor = persistStore(store);

ReactDOM.render(
    <Provider store={ store }>
        <PersistGate loading={null} persistor={ persistor }>
            <ConnectedRouter history={ history }>
                <div>
                    <ConnectedNav />
                    <div className="container"> 
                        <Route exact path="/" component={ Companies } /> 
                        <Route exact path="/company" component={ Companies } />
                        <Route exact path="/company/:id" component={ CompanyDetail } />
                    </div>
                </div>
            </ConnectedRouter>
        </PersistGate>
    </Provider>,
    (document.getElementById("app"): any));