import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from './reducers';
import { watcherSaga } from './sagas.js';

export default history => { 
    const persistConfig = { key: 'root', storage };
    const routeMiddleware = routerMiddleware(history);
    const sagaMiddleware = createSagaMiddleware();
    const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        persistReducer(
            persistConfig,
            rootReducer
        ),
        composeEnchancers(applyMiddleware(routeMiddleware, sagaMiddleware))
    );

    sagaMiddleware.run(watcherSaga);

    return store;
}