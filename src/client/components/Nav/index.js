// @flow
import React from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import './styles.scss';

type Props = { activePath: String };

const links = {
  home: '/',
  about: '/about',
  companies: '/company'
}

const Nav = ({ activePath }: Props) => (
  <nav className="navbar is-primary">
    <div className="navbar-brand">
      <a className="navbar-item">
        Expenses.App
      </a>
      <div className="navbar-burger burger" data-target="navbar">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div id="navbar" className="navbar-menu">
      <div className="navbar-end">
        { Object.entries(links)
            .map(([name, path], index) => (
              <span className="navbar-item" key={index}>
                <Link className={classnames('navbar-item', { 'is-active': activePath === path })} to={path}>{name}</Link>
              </span>
            ))
        }
      </div>
    </div>
  </nav>
)

Nav.setDisplayName = 'Nav';

const mapStateToProps = (state) => ({
  activePath: state.router.location.pathname
})

export default connect(mapStateToProps)(Nav)