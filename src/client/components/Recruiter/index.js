// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddRecruiter from './add';

type Props = { recruiters: [], fetchRecruiters: Function, addRecruiter: Function, deleteRecruiter: Function, companyId: String };
type State = { addingRecruiter: boolean };

class RecruiterList extends Component<Props, State> {
    displayName='Recruiters'
    state = { addingRecruiter: false };

    componentDidMount () {
        this.props.fetchRecruiters(this.props.companyId)
    }

    render () { 
        return (
            <div className="recruiters">
                <button className="button" onClick={ () => this.setState({ addingRecruiter: true }) }>Add Recruiter</button>
                {this.state.addingRecruiter && <AddRecruiter postSubmit={ data => {
                        data.company = this.props.companyId;
                        this.props.addRecruiter(data) ;
                        this.setState({ addingRecruiter: false });
                    }} />  }
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Thoughts</th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.props.recruiters ? this.props.recruiters.map(({ id, name, email, phone, thoughts }, index) => (
                            <tr key={ index }>
                                <td> { name } </td>
                                <td> { email } </td>
                                <td> { phone } </td>
                                <td> { thoughts } </td>
                                <td> <button onClick={(() => this.props.deleteRecruiter(id) )}><i className="fa fa-trash"/></button></td>
                            </tr> ))
                        : <tr><td> No Recruiters </td> </tr>
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = ({ recruiters }) => ({ recruiters: recruiters.recruiters });

const mapDispatchToProps = dispatch => ({
    fetchRecruiters: companyId => dispatch({ type: 'recruiter/get' , companyId }),
    deleteRecruiter: id => dispatch({ type: 'recruiter/delete', payload: id }),
    addRecruiter: payload => dispatch({ type: 'recruiter/add', payload })
})


export default connect(mapStateToProps, mapDispatchToProps)(RecruiterList);