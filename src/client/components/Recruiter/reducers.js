// @flow

import { GET_RECRUITERS_SUCCESS, ADD_RECRUITER_SUCCESS, DELETE_RECRUITER_SUCCESS, ADD_RECRUITER_NOTE_SUCCESS } from './sagas';

const initialState = {};

export default (state: Object = initialState, action: Object) => {
    let recruiters = null;
    switch(action.type) {
        case GET_RECRUITERS_SUCCESS:
            return { ...state, recruiters: action.payload };
        case ADD_RECRUITER_SUCCESS:
            return { ...state, recruiters: [ ...state.recruiters, action.payload]};
        case DELETE_RECRUITER_SUCCESS:
            recruiters = state.recruiters.filter(recruiter => recruiter.id !== action.payload );
            return { ...state, recruiters };
        case ADD_RECRUITER_NOTE_SUCCESS:
            recruiters = state.recruiters.map(recruiter => {
                if (recruiter.id === action.payload.id) {
                    recruiter.notes = action.payload.notes;
                }
                return recruiter
            });
            return { ...state, recruiters };
        default:
            return state;
    }
}