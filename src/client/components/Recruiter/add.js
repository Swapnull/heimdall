// @flow

import * as React from 'react';
import ModalForm from '../Modal/form';

type Props = { postSubmit: Function };

const fields = [
    { name: 'name', type: 'text', extras: { placeholder: "eg. John Smith" } },
    { name: 'email', type: 'email', extras: { placeholder: "eg. john@tech-company.com" } },
    { name: 'phone', type: 'phone', extras: { placeholder: "eg. 07123 455678" } },
    { name: 'thoughts', type:'text', extras: { placeholder: "eg. A nice guy, very chatty" } },
]

const AddRecruiter = ({ postSubmit }: Props) => (
    <ModalForm 
        title="Add Company" 
        fields={fields} 
        postSubmit={postSubmit} />
);

AddRecruiter.setDisplayName = "AddRecruiter"
export default AddRecruiter;