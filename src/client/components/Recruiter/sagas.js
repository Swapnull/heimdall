// @flow

import axios from 'axios';
import { call, put } from 'redux-saga/effects';
import type { Saga } from 'redux-saga'; 

type Payload = { payload: Object };

export const ADD_RECRUITER = 'recruiter/add';
export const ADD_RECRUITER_SUCCESS = 'recruiter/add:success';
export const ADD_RECRUITER_FAILED = 'recruiter/add:failed';
export const DELETE_RECRUITER = 'recruiter/delete';
export const DELETE_RECRUITER_SUCCESS = 'recruiter/delete:success'
export const DELETE_RECRUITER_FAILED = 'recruiter/delete:failed'
export const GET_RECRUITERS = 'recruiter/get';
export const GET_RECRUITERS_SUCCESS = 'recruiter/get:success'
export const GET_RECRUITERS_FAILED = 'recruiter/get:failed'
export const ADD_RECRUITER_NOTE = 'recruiter/addNote';
export const ADD_RECRUITER_NOTE_SUCCESS = 'recruiter/addNote:success';
export const ADD_RECRUITER_NOTE_FAILED = 'recruiter/addNote:failed';

const API_ENDPOINT: string = `${ process.env.API_ENDPOINT || '' }/recruiter`
const config: Object = { headers: { 'Content-Type': 'application/json' } };

export function *fetchRecruiters ({ companyId }: { companyId: string }): Saga<void> {
    try {
        const response = yield call(() => axios.get(API_ENDPOINT));
        const payload = response.data
            .filter(item => item.company === companyId )
            .sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1)
        yield put({ type: GET_RECRUITERS_SUCCESS, payload });
    } catch (err) {
        yield put({ type: GET_RECRUITERS_FAILED, payload: err });
    }
}

export function *deleteRecruiter ({ payload }: Payload): Saga<void> {
    try {
        yield call(id => axios.delete(`${ API_ENDPOINT }/${ id }`), [ payload ]);
        yield put({ type: DELETE_RECRUITER_SUCCESS, payload });
    } catch (err) {
        yield put({ type: DELETE_RECRUITER_FAILED, payload: err });
    }
}

export function *addRecruiter ({ payload }: Payload): Saga<void> {
    try {
        yield call(() => axios.post(API_ENDPOINT, payload, config));
        yield put({ type: ADD_RECRUITER_SUCCESS, payload });
    } catch (err) {
        yield put({ type: ADD_RECRUITER_FAILED, payload: err });
    }
}