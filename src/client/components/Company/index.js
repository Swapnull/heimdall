// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AddCompany from './add';
import { connect } from 'react-redux';

type Props = { fetchCompanies: Function, addCompany: Function, deleteCompany: Function, 
    data: [{ id: string, name: string, phone: string, email: string }] };
type State = { addingNew: boolean };

class Companies extends Component<Props, State> {
    displayName='Companies';

    constructor () {
        super();
        this.state = { addingNew: false };
    }

    componentDidMount () {
        this.props.fetchCompanies();
    }

    render () { 
        return (
            <div>
                <button className="button is-primary" onClick={() => this.setState({ addingNew: true }) }>
                    <span className="icon"><i className="fa fa-users"/></span>
                    <span>Add Company</span>
                </button>
                <table className="table is-striped is-fullwidth">
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Phone </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        { this.props.data && this.props.data.map(({ id, name, phone }) => (
                        <tr key={ id }> 
                            <td> <Link to={`/company/${ id }`}>{ name } </Link> </td>
                            <td> { phone } </td>
                            <td> <button onClick={(() => this.props.deleteCompany(id) )}><i className="fa fa-trash"/></button></td>
                        </tr>
                        )) }
                    </tbody>
                </table>
                { this.state.addingNew && <AddCompany postSubmit={(data) => {
                    this.props.addCompany(data);
                    this.setState({ addingNew: false });
                }
                } /> }
            </div>)
    }
 }


const mapStateToProps = ({ companies }) => ({ data: companies.companies })

const mapDispatchToProps = dispatch => ({
    fetchCompanies: () => dispatch({ type: 'company/get' }),
    deleteCompany: id => dispatch({ type: 'company/delete', payload: id }),
    addCompany: payload => dispatch({ type: 'company/add', payload })
})

export default connect(mapStateToProps, mapDispatchToProps)(Companies);