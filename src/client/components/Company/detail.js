// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notes from '../Note/';
import Jobs from '../Job/';
import Recruiters from '../Recruiter/';

import classnames from 'classnames';

type Company = { id: String, name: String, phone: String, email: String, notes: [{ note: String, created: String }] };
type Props = { fetchCompanies: Function, addNote: Function, data: [Company], match: { params: { id: String } }, updateSelectedTab: Function };
type State = { selectedTab: string }

class CompanyDetail extends Component <Props, State> {
    tabs = [ 'Notes', 'Recruiters', 'Jobs'];
    state = { selectedTab: 'notes' };

    componentDidMount () {
        this.props.fetchCompanies();
    }

    render () { 
        const company: ?Company = this.props.data.find(({ id }: { id: String }): boolean => this.props.match.params.id === id );
        if (company) {
            const { id, name , phone, notes } = company; 
            return (
                <div>
                    <h1 className="name is-size-1">{ name }</h1>
                    <div className="phone">{ phone }</div>
                    <div className="tabs">
                        <ul>
                            { this.tabs.map((tab, index) => (
                                <li key={index} className={classnames({ 'is-active': this.props.selectedTab === tab }) }>
                                    <a onClick={() => this.props.updateSelectedTab(tab)}>{tab}</a>
                                </li>
                            ))}
                        </ul>
                    </div>
                    { this.state.selectedTab === 'notes' && <Notes notes={ notes } addNote={ notes => this.props.addNote(company, notes) } /> }
                    { this.state.selectedTab === 'recruiters' && <Recruiters companyId={ id } /> }
                    { this.state.selectedTab === 'jobs' && <Jobs companyId={ id } /> }
                </div>
            );
        } else {
            return ( <div> No Companies Found </div> );
        }
    }
 }

const mapStateToProps = ({ companies }) => ({ 
    data: companies.companies,
    selectedTab: companies.selectedTab
})

const mapDispatchToProps = dispatch => ({
    fetchCompanies: () => dispatch({ type: 'company/get' }),
    addNote: (company, notes) => dispatch({ type: 'company/addNote', payload: { ...company, notes }  }),
    updateSelectedTab: tab => dispatch({ type: 'company/updateSelectedTab', tab }) 
})

export default connect(mapStateToProps, mapDispatchToProps)(CompanyDetail);