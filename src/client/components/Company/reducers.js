// @flow

import { GET_COMPANIES_SUCCESS, ADD_COMPANY_SUCCESS, DELETE_COMPANY_SUCCESS, 
    ADD_COMPANY_NOTE_SUCCESS, UPDATE_COMPANY_SELECTED_TAB_SUCCESS } from './sagas';

const initialState = {};

export default (state: Object = initialState, action: Object) => {
    let companies = null;
    switch(action.type) {
        case GET_COMPANIES_SUCCESS:
            return { ...state, companies: action.payload };
        case ADD_COMPANY_SUCCESS:
            return { ...state, companies: [ ...state.companies, action.payload ]};
        case DELETE_COMPANY_SUCCESS:
            companies = state.companies.filter(company => company.id !== action.payload );
            return { ...state, companies };
        case ADD_COMPANY_NOTE_SUCCESS:
            companies = state.companies.map(company => {
                if (company.id === action.payload.id) {
                    company.notes = action.payload.notes;
                }
                return company
            });
            return { ...state, companies };
        case UPDATE_COMPANY_SELECTED_TAB_SUCCESS:
            return { ...state, selectedTab: action.payload };
        default:
            return state;
    }
}