// @flow
import React from 'react';
import ModalForm from '../Modal/form';
    
type Props = { postSubmit: Function  };

const fields = [
    { name: 'name', type: 'text', extras: { placeholder: "eg. TechRecruit" } },
    { name: 'phone', type: 'tel', extras: { placeholder: "eg. 07123456789 or (+44)7123 456789" } },
]

const AddCompany = ({ postSubmit }: Props) => (
    <ModalForm 
        title="Add Company" 
        fields={fields} 
        postSubmit={postSubmit} />
);

AddCompany.setDisplayName = "AddCompany"
export default AddCompany;