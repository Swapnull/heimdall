// @flow

import axios from 'axios';
import { call, put } from 'redux-saga/effects';
import type { Saga } from 'redux-saga'; 

type Payload = { payload: Object };

export const ADD_COMPANY = 'company/add';
export const ADD_COMPANY_SUCCESS = 'company/add:success';
export const ADD_COMPANY_FAILED = 'company/add:failed';
export const DELETE_COMPANY = 'company/delete';
export const DELETE_COMPANY_SUCCESS = 'company/delete:success'
export const DELETE_COMPANY_FAILED = 'company/delete:failed'
export const GET_COMPANIES = 'company/get';
export const GET_COMPANIES_SUCCESS = 'company/get:success'
export const GET_COMPANIES_FAILED = 'company/get:failed'
export const ADD_COMPANY_NOTE = 'company/addNote';
export const ADD_COMPANY_NOTE_SUCCESS = 'company/addNote:success';
export const ADD_COMPANY_NOTE_FAILED = 'company/addNote:failed';
export const UPDATE_COMPANY_SELECTED_TAB = 'company/updateSelectedTab'
export const UPDATE_COMPANY_SELECTED_TAB_SUCCESS = 'company/updateSelectedTab:success'

export const UPDATE_COMPANY_SELECTED_TAB_FAILED = 'company/updateSelectedTab:failed'


const API_ENDPOINT: string = `${ process.env.API_ENDPOINT || '' }/company`
const config: Object = { headers: { 'Content-Type': 'application/json' } };

export function *fetchCompanies (): Saga<void> {
    try {
        const response = yield call(() => axios.get(API_ENDPOINT));
        const payload = response.data.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1)
        yield put({ type: GET_COMPANIES_SUCCESS, payload });
    } catch (err) {
        yield put({ type: GET_COMPANIES_FAILED, payload: err });
    }
}

export function *deleteCompany ({ payload }: Payload): Saga<void> {
    try {
        yield call(id => axios.delete(`${ API_ENDPOINT }/${ id }`), [ payload ]);
        yield put({ type: DELETE_COMPANY_SUCCESS, payload });
    } catch (err) {
        yield put({ type: DELETE_COMPANY_FAILED, payload: err });
    }
}

export function *addCompany ({ payload }: Payload): Saga<void> {
    try {
        yield call(() => axios.post(API_ENDPOINT, payload, config));
        yield put({ type: ADD_COMPANY_SUCCESS, payload });
    } catch (err) {
        yield put({ type: ADD_COMPANY_FAILED, payload: err });
    }
}

export function *addNote ({ payload }: Payload): Saga<void> {
    try {
        yield call(() => axios.post(`${ API_ENDPOINT }`, payload, config));
        yield put({ type: ADD_COMPANY_NOTE_SUCCESS, payload });
    } catch (err) {
        yield put({ type: ADD_COMPANY_NOTE_FAILED, payload: err });
    }
}

export function *updateSelectedTab({ tab }: { tab: string }): Saga<void> {
    try {
        console.log(tab);
        yield put({ type: UPDATE_COMPANY_SELECTED_TAB_SUCCESS, payload: tab })
    } catch (err) {
        yield put({ type: UPDATE_COMPANY_SELECTED_TAB_FAILED, payload: err })
    }
}