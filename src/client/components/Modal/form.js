// @flow
import React, { Component } from 'react';
import Modal from './index';

type Props = { title: string, fields: Array<{ name: string, type: string, extras: Object }>, postSubmit: Function }
    
export default class ModalForm extends Component<Props> {
    displayName="ModalForm";
    form = React.createRef();

    constructor() {
        super();
        (this: any).handleChange = this.handleChange.bind(this);
        (this: any).handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange (event: Object) {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleSubmit (event: Object) { 
        event.preventDefault(); 
        this.props.postSubmit(this.state);
    }

    render () {
        const { title, fields } = this.props;
        return (
            <Modal title={title} onModalSubmit={this.handleSubmit} >
                <form ref={this.form}>
                    <div className="field">
                        { fields.map(({ name, type, extras }, index) => (
                            <div className="field control is-expanded" key={ index }>
                                <label className="label">{name}</label>
                                <input className="input" type={ type } autoComplete={ name } name={ name }
                                    onChange={this.handleChange} {...extras }/>
                            </div>
                        ))}
                    </div>
                </form> 
            </Modal>
        )
    }
}


