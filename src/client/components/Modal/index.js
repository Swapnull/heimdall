// @flow
import React from 'react';
import type { Node } from 'react';
    
type Props = { title: string, children: Node, onModalSubmit: Function };
const Modal = ({ title, children, onModalSubmit }: Props) =>  (
    <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card">
            <header className="modal-card-head">
                <p className="modal-card-title">{ title }</p>
                <button className="delete" aria-label="close"></button>
            </header>
            <section className="modal-card-body">
                { children }
            </section>
            <footer className="modal-card-foot">
                <button className="button is-primary" onClick={onModalSubmit}>Save changes</button>
                <button className="button is-danger">Cancel</button>
            </footer>
        </div>
    </div>
);

Modal.setDisplayName = 'Modal';
export default Modal;


