// @flow

import * as React from 'react';
import ModalForm from '../modal/form';

type Props = { postSubmit: Function };

const fields = [
    { name: 'name', type: 'text', extras: { placeholder: "eg. TechJob" } },
    { name: 'location', type: 'text', extras: { placeholder: "eg. London, Barcelona" } },
    { name: 'rate', type: 'text', extras: { placeholder: "eg. 400 per day" } },
    { name: 'status', type:'text', extras: { placeholder: "eg. Active" } },
]

const AddJob = ({ postSubmit }: Props) => (
    <ModalForm 
        title="Add Company" 
        fields={fields} 
        postSubmit={postSubmit} />
);

AddJob.setDisplayName = "AddJob"
export default AddJob;