// @flow

import axios from 'axios';
import { call, put } from 'redux-saga/effects';
import type { Saga } from 'redux-saga'; 

type Payload = { payload: Object };

export const ADD_JOB = 'job/add';
export const ADD_JOB_SUCCESS = 'job/add:success';
export const ADD_JOB_FAILED = 'job/add:failed';
export const DELETE_JOB = 'job/delete';
export const DELETE_JOB_SUCCESS = 'job/delete:success'
export const DELETE_JOB_FAILED = 'job/delete:failed'
export const GET_JOBS = 'job/get';
export const GET_JOBS_SUCCESS = 'job/get:success'
export const GET_JOBS_FAILED = 'job/get:failed'
export const ADD_JOB_NOTE = 'job/addNote';
export const ADD_JOB_NOTE_SUCCESS = 'job/addNote:success';
export const ADD_JOB_NOTE_FAILED = 'job/addNote:failed';

const API_ENDPOINT: string = `${ process.env.API_ENDPOINT || '' }/job`
const config: Object = { headers: { 'Content-Type': 'application/json' } };

export function *fetchJobs ({ companyId }: { companyId: string }): Saga<void> {
    try {
        const response = yield call(() => axios.get(API_ENDPOINT));
        const payload = response.data
            .filter(item => item.company === companyId )
            .sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1)
        yield put({ type: GET_JOBS_SUCCESS, payload });
    } catch (err) {
        yield put({ type: GET_JOBS_FAILED, payload: err });
    }
}

export function *deleteJob ({ payload }: Payload): Saga<void> {
    try {
        yield call(id => axios.delete(`${ API_ENDPOINT }/${ id }`), [ payload ]);
        yield put({ type: DELETE_JOB_SUCCESS, payload });
    } catch (err) {
        yield put({ type: DELETE_JOB_FAILED, payload: err });
    }
}

export function *addJob ({ payload }: Payload): Saga<void> {
    try {
        yield call(() => axios.post(API_ENDPOINT, payload, config));
        yield put({ type: ADD_JOB_SUCCESS, payload });
    } catch (err) {
        yield put({ type: ADD_JOB_FAILED, payload: err });
    }
}

export function *addNote ({ payload }: Payload): Saga<void> {
    try {
        yield call(() => axios.post(`${ API_ENDPOINT }`, payload, config));
        yield put({ type: ADD_JOB_NOTE_SUCCESS, payload });
    } catch (err) {
        yield put({ type: ADD_JOB_NOTE_FAILED, payload: err });
    }
}