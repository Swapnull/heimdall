// @flow
import React, { Component } from 'react';
import AddJob from './add';
import { connect } from 'react-redux';
import { DateTime } from 'luxon';

type Job = { id: String, name: String, location: String, rate: String, status: String, updated: String, notes: [{ note: String, created: String }] }
type Props = { fetchJobs: Function, deleteJob: Function, addJob: Function, jobs: [Job], companyId: String }
type State = { addingJob: boolean }

class JobList extends Component<Props, State> {

    constructor () {
        super();
        this.state = { addingJob: false };
    }

    componentDidMount () {
        this.props.fetchJobs(this.props.companyId);
    }

    render () {
        return (
            <div>
                <button className="button" onClick={ () => this.setState({ addingJob: true })}>Add Job</button>
                {this.state.addingJob && <AddJob postSubmit={ data => {
                        data.company = this.props.companyId;
                        this.props.addJob(data) ;
                        this.setState({ addingJob: false });
                    }} />  }
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last Updated</th>
                            <th>Location</th>
                            <th>Rate</th>
                            <th>Status</th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.props.jobs ? this.props.jobs.map((job, index) =>
                            <tr key={index}>
                                <td> {job.name} </td>
                                <td> { DateTime.fromISO(job.updated).toLocaleString() } </td>
                                <td> {job.location} </td>
                                <td> {job.rate} </td>
                                <td> {job.status} </td>
                                <td> <button onClick={(() => this.props.deleteJob(job.id) )}><i className="fa fa-trash"/></button></td>
                            </tr> )
                        : <tr><td> No Jobs </td> </tr>
                        }
                    </tbody>
                </table>
            </div>
            )
    }
}

const mapStateToProps = ({ jobs }) => ({ jobs: jobs.jobs });

const mapDispatchToProps = dispatch => ({
    fetchJobs: companyId => dispatch({ type: 'job/get', companyId }),
    deleteJob: id => dispatch({ type: 'job/delete', payload: id }),
    addJob: payload => dispatch({ type: 'job/add', payload })
})


export default connect(mapStateToProps, mapDispatchToProps)(JobList);