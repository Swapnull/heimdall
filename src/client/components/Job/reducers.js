// @flow

import { GET_JOBS_SUCCESS, ADD_JOB_SUCCESS, DELETE_JOB_SUCCESS, ADD_JOB_NOTE_SUCCESS } from './sagas';

const initialState = {};

export default (state: Object = initialState, action: Object) => {
    let jobs = null;
    switch(action.type) {
        case GET_JOBS_SUCCESS:
            return { ...state, jobs: action.payload };
        case ADD_JOB_SUCCESS:
            return { ...state, jobs: [ ...state.jobs, action.payload]};
        case DELETE_JOB_SUCCESS:
            jobs = state.jobs.filter(job => job.id !== action.payload );
            return { ...state, jobs };
        case ADD_JOB_NOTE_SUCCESS:
            jobs = state.jobs.map(job => {
                if (job.id === action.payload.id) {
                    job.notes = action.payload.notes;
                }
                return job
            });
            return { ...state, jobs };
        default:
            return state;
    }
}