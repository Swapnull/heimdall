// @flow

import * as React from 'react';
import { DateTime } from 'luxon';

type Props = { notes: [{ note: String, created: String }],  addNote: Function };
type State = { note: string };

class AddNote extends React.Component <Props, State> {
    displayName="AddNote";

    constructor() {
        super();
        this.state = { note: '' };
        (this: any).handleChange = this.handleChange.bind(this);
        (this: any).handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (event: Object) {
        this.setState({ note: event.target.value })
    }

    handleSubmit (event: Object) { 
        event.preventDefault();
        const note = { note: this.state.note, created: DateTime.local().toString() }
        const notes = this.props.notes ? [...this.props.notes, note ]: [note];
        this.props.addNote(notes);
    }

    render () {     
        return ( 
            <form onSubmit={this.handleSubmit}>
                <div className="field">
                    <div className="control">
                        <textarea className="textarea" type="text" name="note"
                            onChange={this.handleChange} placeholder="Add your note here..."/>
                    </div>
                    <input className="button" type="submit" value="Save"/>       
                </div>
            </form> 
        );
    }
}
export default AddNote;