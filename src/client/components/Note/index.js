// @flow
import React, { Component } from 'react';
import AddNote from './add';
import { DateTime } from 'luxon';

type Props = {
    notes: [{ note: String, created: String }],
    addNote: Function
}

type State = {
    addingNote: boolean
}

class Notes extends Component<Props, State> {
    displayName='Notes'

    constructor () {
        super();
        this.state = { addingNote: false };
    }

    render () { 
        const { notes } = this.props;
        return (
            <div className="notes">
                <button className="button" onClick={ () => this.setState({ addingNote: true }) }>Add Note</button>
                { notes && <ul>{ notes.map(({ note, created }, index) => <li key={ index }> { DateTime.fromISO(created).toLocaleString(DateTime.DATETIME_SHORT) } - { note } </li>) } </ul> }
                { this.state.addingNote && <AddNote 
                notes={ notes }
                addNote={notes=> { 
                    this.props.addNote(notes); 
                    this.setState({ addingNote: false }); 
                }}/> }
            </div>
        );
    }
}

export default Notes;